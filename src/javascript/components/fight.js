import { controls } from '../../constants/controls';
import {fighters} from "../helpers/mockData";

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const fighters = setAdditionalFighterStats(firstFighter, secondFighter);
    let actionKeys = {
      KeyA: false,
      KeyJ: false,
      KeyD: false,
      KeyL: false,
      KeyQ: false,
      KeyW: false,
      KeyE: false,
      KeyU: false,
      KeyI: false,
      KeyO: false
    };

    document.addEventListener('keyup', function (event) {
      const keyPressed = event.code;

      if (actionKeys.hasOwnProperty(keyPressed)) {
        const [action, activeFighter] = getFighterAction(actionKeys);
        actionKeys[keyPressed] = false;
        if (action === 'block')
          changeDefenceStance(fighters, activeFighter);
      }
    })

    document.addEventListener('keydown', function (event) {
      const keyPressed = event.code;

      if (actionKeys.hasOwnProperty(keyPressed)) {
        actionKeys[keyPressed] = true;
        const [action, activeFighter] = getFighterAction(actionKeys);
        if (action !== '') {
          makeAction(action, fighters, activeFighter);
          renderHealth(fighters);
        }
        //Окончания боя при здоровье одного из бойцов <= 0
        if (typeof getWinner(fighters) === 'object')
          resolve(getWinner(fighters));
      }
    })
  });
}

function setAdditionalFighterStats (firstFighter, secondFighter) {
  if (firstFighter === secondFighter)
    secondFighter =  Object.assign({}, secondFighter);
  const fightersWithAdditionalStats = [firstFighter, secondFighter];
  fightersWithAdditionalStats.forEach((fighter, index) => {
    fighter['defenceStance'] = false;
    fighter['maxHealth'] = fighter['health'];
    fighter['lastCritTime'] = 0;
    fighter['player'] = index + 1;
  })
  return fightersWithAdditionalStats;
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage > 0 && defender['defenceStance'] === false && attacker['defenceStance'] === false)
    return damage;
  else
    return 0;
}

function getCriticalDamage(attacker) {
  const critDelaySeconds = 10000;
  if (Date.now() > (attacker['lastCritTime'] + critDelaySeconds)) {
    attacker['lastCritTime'] = Date.now()
    return attacker['attack'] * 2;
  } else
    return 0;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  return fighter['attack'] * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const criticalHitChance = Math.random() + 1;
  return fighter['defense'] * criticalHitChance;
}

//Возвращает действие которое будет выполнено бойцом и бойца, который его выполняет
function getFighterAction (actionKeys) {
  if (actionKeys['KeyA'] === true) {
    return ['hit', 0]
  } else if (actionKeys['KeyD'] === true) {
    return ['block', 0]
  } else if (actionKeys['KeyJ'] === true) {
    return ['hit', 1]
  } else if (actionKeys['KeyL'] === true) {
    return ['block', 1]
  } else if (actionKeys['KeyQ']===true && actionKeys['KeyW']===true && actionKeys['KeyE']===true) {
    return ['critical', 0]
  } else if (actionKeys['KeyU']===true && actionKeys['KeyI']===true && actionKeys['KeyO']===true) {
    return ['critical', 1]
  } else
    return ['', '']
}

function makeAction(action, fighters, activeFighter) {
  let [attacker, defender] = setFighterSides(fighters, activeFighter);

  switch (action){
    case 'block':
      changeDefenceStance(fighters, activeFighter);
      break;
    case 'hit':
      defender['health'] -= getDamage(attacker, defender);
      break;
    case 'critical':
      defender['health'] -= getCriticalDamage(attacker);
      break;
    default:
      break;
  }
}

function changeDefenceStance (fighters, activeFighter) {
  fighters[activeFighter]['defenceStance'] = fighters[activeFighter]['defenceStance'] === false;
}

function setFighterSides (fighters, activeFighter) {
  switch (activeFighter) {
    case 0:
      return [fighters[0], fighters[1]];
    case 1:
      return [fighters[1], fighters[0]];
  }
}

function renderHealth (fighters) {
  const firstFighterHealthPercents = fighters[0]['health'] / fighters[0]['maxHealth'] * 100;
  const secondFighterHealthPercents = fighters[1]['health'] / fighters[1]['maxHealth'] * 100;

  document.getElementById("left-fighter-indicator").style.width = `${firstFighterHealthPercents}%`;
  document.getElementById("right-fighter-indicator").style.width = `${secondFighterHealthPercents}%`;
}

function getWinner(fighters) {
  if (fighters[0]['health'] <= 0) {
    return fighters[1];
  } else if (fighters[1]['health'] <= 0) {
    return fighters[0];
  } else
    return false;
}

