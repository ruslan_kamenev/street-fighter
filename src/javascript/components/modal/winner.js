import {showModal} from "./modal";
import {createFighterImage} from "../fighterPreview";

export function showWinnerModal(fighter) {
  // call showModal function
    const winner = {}
    winner['title'] = `Игрок ${fighter['player']} победил`;
    winner['bodyElement'] = createFighterImage(fighter);
    winner['onClose'] = () => {window.location.href = "/"};

    showModal(winner);
}
