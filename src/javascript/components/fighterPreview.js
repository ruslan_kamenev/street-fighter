import {createElement} from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (typeof fighter !== 'undefined') {
    fighterElement.innerHTML =  (createFighterDescription(fighter))
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterDescription(fighter) {
  let descriptionElement = createElement( {
    tagName: 'div',
    className: 'fighter-description'
  })

  descriptionElement.innerHTML = createFighterDescriptionText(fighter)
  let fighterDescriptionImg = createFighterImage(fighter)
  return fighterDescriptionImg.outerHTML + descriptionElement.outerHTML;
}

function createFighterDescriptionText (fighter) {
  let fields = ['name', 'health', 'attack', 'defense']
  let elementInfo = ''
  Object.keys(fighter).forEach(function (key) {
    if (fields.includes(key)) {
      elementInfo += `${key.charAt(0).toUpperCase()+key.slice(1)} : ${fighter[key]} <br>`
    }
  })
  return elementInfo;
}

