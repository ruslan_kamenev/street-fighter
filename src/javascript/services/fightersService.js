import { callApi } from '../helpers/apiHelper';

class FighterService {
  #endpoint = 'fighters.json';

  async getFighters() {
    try {
      const apiResult = await callApi(this.#endpoint);
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    // todo: implement this method
    let fighterEndpoint = `details/fighter/${id}.json`;
    try {
      let apiResult = await callApi(fighterEndpoint);
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

}

export const fighterService = new FighterService();